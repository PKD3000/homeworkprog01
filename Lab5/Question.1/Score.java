import java.util.*;
import java.io.*;

/* This program reads names and scores from a file and reports back the best and
 the worst grades with the students' names, the amount of scores checked and the score average*/

public class Score {
    
    public static void main(String[] args) throws FileNotFoundException {
       
        File scores = new File("scores.txt"); // using File to make java use a file
        Scanner namesc = new Scanner(scores); // using scanner to read form file
        // some vars
        double biggestScore = 0;
        double smallestScore = 0;
        String bestStudent = " ";
        String worstStudent = " ";
        boolean firstScore = false;
        double sumOfScores = 0;
        double amountOfScores = 0;

        //start of the main function
        while(namesc.hasNextLine()){ // making a loop thats sees if there's a other line if so it continues
            String name = namesc.next();
            double score = namesc.nextDouble();
            if(!firstScore){ //putting the first score as base for the smallest number
                smallestScore = score;
                firstScore = true;
            }
            if(score > biggestScore){// this part checks if the score is bigger than the value of biggestScore if so it put the score and the student name in vars
                biggestScore = score;
                bestStudent = name;
            }if(score < smallestScore){// this part checks if the score is smaller than the value of smallestScore if so it put the score and the student name in vars
                smallestScore = score;
                worstStudent = name;
            }
            sumOfScores += score; // used for the average later on
            amountOfScores += 1; // counts the numeber of scores processed
            System.out.println(name + " " + score);
        }
        namesc.close();
        double scoreAverage = sumOfScores / amountOfScores; // calculates the average
        
        System.out.println("The student with the best score is " + bestStudent + " with the score of " + biggestScore);
        System.out.println("The student with the worst score is " + worstStudent + " with the score of " + smallestScore);
        System.out.println("Scores processed: " + amountOfScores);
        System.out.println("Scores average: " + scoreAverage);
    }

}