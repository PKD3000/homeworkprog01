import java.util.*;
/* This program get two intergers form user input and find their GCD(greatest common divisor) */
public class Ged {

    public static void main(String[] args){
        int number1 = inputter(1);
        int number2 = inputter(2);
        int num1 = number1;
        int num2 = number2;

        if(number1 > 0 && number2 > 0){ //removes the default return form inputter
            while(number1 != number2){ //Here we find the GCD by substacting the bigger number by the smaller one until they are equal there they will be equal to the GCD
                if(number1 > number2){
                    number1 = number1 - number2;
                }else{
                    number2 = number2 - number1;
                }
            }
            System.out.println("The greatest common divisor of " + num1 + " and " + num2 + " is: " + number2);
        }
    }
    private static int inputter(int n){
        try{// try catch is used to catch the error if the input is not and integer there it just restarts the program
            Scanner numberSc = new Scanner(System.in);
            if(n == 1){
                System.out.print("enter the first integer: ");
            }else{
                System.out.print("enter the second integer: ");
            }
            int number = numberSc.nextInt();
            if(number == 0){
                System.out.println("There's no greatest common divisor if one number is equal to: 0");
                System.exit(1);
            }
            return(number);
        }catch(InputMismatchException e){
            System.out.println("The input has to be an integer\ntry again");
            main(null);
        }
        return(0); // this return is a must for the try method to work


    }


}