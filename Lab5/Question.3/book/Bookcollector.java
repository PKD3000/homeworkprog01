package book;
/* This program adds five books as Book objects and then prints all the information of one book,
it prints the number of books added, it checks if  two books have the same ISBN as two other book. 
It also checks if a book has the same publicher as a other book*/
public class Bookcollector {
    
    public static void main(String[] args){
        //making new Book objects
        Book luck = new Book("Luck", "Julie Legard", "895-7-83-146745-2", 2002);
        Book robe = new Book("The House of Robe", "Roger Louis", "675-7-68-942345-8", 2012);
        Book rodeo = new Book("The Rodeo Scare", "John Doe", "895-7-83-146745-2", 1956);
        Book glory = new Book("Lost Glory", "Sophia bigghear", "565-7-68-850945-8", 2012);
        Book road = new Book("Road From Nowhere", "Steward Koolman", "153-6-89-789995-1", 1956);

        //printing all the information of robe
        System.out.println(robe.result());
        //prints the amount of books added
        System.out.println("Book count: " + Book.bookCount);
        //prints if the two books have the same ISBN
        System.out.println("The Rodeo Scare and Luck have the same ISBN: " + rodeo.match(luck));
        System.out.println("Lost Glory and Luck have the same ISBN: " + glory.match(luck));
        //prints if the two books have the same publicher
        System.out.println("Lost Glory and Road From nowhere have the same publicher: " + glory.publicherMatch(road));

    }

}