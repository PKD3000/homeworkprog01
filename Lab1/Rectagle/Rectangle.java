import java.util.*;

public class Rectangle{
	
	public static void main(String[] args){
		Scanner strWidth = new Scanner(System.in);
		System.out.print("Width:");
		double Width = strWidth.nextDouble();
		
		Scanner strLength = new Scanner(System.in);
		System.out.print("Length:");
		double Length = strLength.nextDouble();
		
		double Perimeter = Width * 2 + Length * 2;
		double Area = Width * Length;
		
		System.out.println("the " + Width + " X " + Length + " rectangle has a perimeter of " + Perimeter + " units and an area of " + Area + " units");
		
		
		
		
	}
	
}