import java.util.*;


public class AreaOfCircle{
	
	public static void main(String[] args){
		Scanner Radsca = new Scanner(System.in);
		final double PI = 3.14159;
		System.out.print("Radius: ");
		double radius = Radsca.nextDouble();
		
		double area = PI * radius * radius;
		System.out.println("The area of the circle is: " + area);
		
		
	}
	
	
	
}