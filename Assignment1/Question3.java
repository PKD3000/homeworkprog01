import java.util.*;

public class Question3{
	/* This program adds up the 3 digits form a number given by the user */

	public static void main(String[] args){
		int digit = scanner();
		int n = digit;
		int sum = 0;

		while(n != 0){
			sum = sum + n % 10;
			n = n / 10;
		}
		System.out.println("Number: " + digit);
		System.out.println("Sum: " + sum);
		
	}
	private static int scanner(){
		Scanner scGivenInterger = new Scanner(System.in);
		System.out.print("Enter a whole number of 3 digits: ");

		int givenInterger = scGivenInterger.nextInt();

		return(givenInterger);

	}

}