import java.util.*;

public class Question2{
	/* This program gives the TPS TVQ TIP and the total price
	 for a product */

	public static void main(String[] args){
		
		double productPrice = scanner();
		double tps = productPrice * 0.05;
		double tvq = productPrice * 0.09975;
		double tip = productPrice * 0.15;

		double totalPrice = productPrice + tps + tvq + tip;

		System.out.println("Product price: " + String.format("%.2f", productPrice) + "$");
		System.out.println("TPS: " + String.format("%.2f", tps) + "$");
		System.out.println("TVQ: " + String.format("%.2f", tvq) + "$");
		System.out.println("Tip: " + String.format("%.2f", tip) + "$");
		System.out.println("Total: " + String.format("%.2f", totalPrice) + "$");
		
	}

	private static double scanner(){
		Scanner scProductPrice = new Scanner(System.in);
		System.out.print("Enter the product price in CAD: ");

		double productPrice = scProductPrice.nextDouble();
		// Returning productPrice
		return(productPrice);

	}


}