import java.util.*;

public class Question4{
	/* this program prompts the user for a url, removes all whitespace form it
	and checks if it's secure. */

	public static void main(String[] args){

		String url = scanner();
		String safeUrl = "https://";
		boolean safe = false;
		//removing all whitespaces from url
		url = url.replaceAll("\\s", "");

		System.out.println("Verifying an URL with " + url.length() + " characters");
		//Checking if url starts with https://
		if(url.startsWith(safeUrl)){
			safe = true;

		}
		System.out.println(url + " Is this a valid and secure URL ? " + safe);

	}
	private static String scanner(){
		Scanner scUrl = new Scanner(System.in);
		System.out.print("Enter safe URL: ");
		String url = scUrl.next();

		return(url);

	}


}