import java.util.*;
import java.lang.*;

public class Question5{
	/* This program generates a random number and then prints the number, calculates and prints:
	the power(2, 3), the square root, the natural logarithm and the base 10 logarithm of the number */

	public static void main(String[] args){
		//Variables
		double num = randomNum();
		/* The use of Math lets us do the powers and the logarithms of the number */
		double powerNum = Math.pow(num, 2);
		double powerNum2 = Math.pow(num, 3);
		double sqrtNum = Math.sqrt(num);
		double logENum = Math.log(num);
		double log10Num = Math.log10(num);


		System.out.println("The random number is: " + String.format("%.2f", num));
		System.out.println("The power of the number is: " + String.format("%.2f", powerNum));
		System.out.println("The power(3) of the number is: " + String.format("%.2f", powerNum2));
		System.out.println("The square root of the number is: " + String.format("%.2f", sqrtNum));
		System.out.println("The natural logarithm of the number is: " + String.format("%.2f", logENum));
		System.out.println("The base 10 logarithm of the number is: " + String.format("%.2f", log10Num));
		
	}

	private static int randomNum(){
		//Generating a random number between 0 - 100
		Random rdNum = new Random();
		int num = rdNum.nextInt(101);

		return(num);

	}


}