//importing java.util.ALL
import java.util.*;

public class Question1{
	/* This program calculates the total cost of a roadtrip 
	by taking in acount of the average kilometers per liter,
	gas price and the total kilometers planned */

	public static void main(String[] args){

	//Variables
		double gasPrice = 1.16;
		double totalKilometers = scanner(1);
		double kilometersPerLiter = scanner(2);

		double totalPrice = (totalKilometers / kilometersPerLiter) * gasPrice;
		//String.format("%.2f", totalPrice) makes it so it prints only 2 digits after decimal point
		System.out.print("The roadtrip total cost is: " + String.format("%.2f", totalPrice));

	}//scanner(int n) makes it so when scanner() is called you can pass a value to n
	private static double scanner(int n){
		if(n == 1){
			Scanner scTotalKilometers = new Scanner(System.in);
			System.out.print("Kilometers planned to travel: ");

			double totalKilometers = scTotalKilometers.nextDouble();
			//Returning value of totalKilometers
			return(totalKilometers);

		}else if(n == 2){
			Scanner scKilometersPerLiter = new Scanner(System.in);
			System.out.print("Kilometers per liter: ");

			double kilometersPerLiter = scKilometersPerLiter.nextDouble();
			//Returning value of kilometersPerLiter
			return(kilometersPerLiter);


		}else{
			//Returning value 0 only if n isn't equal to 1 or 2
			return(0);

		}


	}

}