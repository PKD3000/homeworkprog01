import java.util.*;

public class Name{

	public static void main(String[] args){
		Scanner userName= inputer();
		String lastName = userName.next();
		String firstName = userName.next();

		firstName = firstName.replaceAll("\\s*,\\s*$", "");
		lastName = lastName.replaceAll("\\s*,\\s*$", "");
		int firstLength = firstName.length();
		int lastLength = lastName.length();

		firstName = firstName.substring(0,1).toUpperCase() + firstName.substring(1,firstLength).toLowerCase();
		lastName = lastName.substring(0,1).toUpperCase() + lastName.substring(1,lastLength).toLowerCase();


		System.out.println(firstName + " " + lastName);


	}

	private static Scanner inputer(){
		Scanner name = new Scanner(System.in);
		System.out.println("Enter your full name in lastname, firstname format with the comma.");
		System.out.print("Name: ");
		return(name);

	}

}
