import java.util.*;
import java.lang.*

public class BmiCal{

	public static void main(String[] args){
		double kg = converter(1);
		double meters = converter(2);
		double bmi;

		bmi = kg/(Math.pow(meters));
		System.out.println("Your BMI is " + bmi);

	}
	private static double inputer(int n){
		if(n == 1){
			Scanner scPounds = new Scanner(System.in);
			System.out.print("Enter your weight in pounds: ");
			double pounds = scPounds.nextDouble();
			return(pounds);

		}
		else if(n == 2){
			Scanner scFeet = new Scanner(System.in);
			System.out.print("Enter your height in feet: ");
			double feet = scFeet.nextDouble();
			return(feet);

		}else{
			return(0);

		}

	}
	private static double converter(int n){
		final double FEETTOMETERS = 0.3048;
		final double POUNDSTOKG = 0.453592;

		if(n == 1){
			double pounds = inputer(1);
			double kg = pounds * POUNDSTOKG;
			return(kg);

		}else if(n == 2){
			double feet = inputer(2);
			double meters = feet * FEETTOMETERS;
			return(meters);

		}else{
			return(0);

		}

	}
}
