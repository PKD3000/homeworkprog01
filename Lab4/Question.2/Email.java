import java.util.*;
/*this program validate an email inputted by the user 
by checking if it has a '@' a dot and if it ends by something else than '.'.*/

public class Email{

	public static void main(String[] args){
		validator();

	}
	private static String inputter(){
		Scanner scEmail = new Scanner(System.in);
		System.out.print("Your Email: ");
		String email = scEmail.next();
		return(email);

	}
	private static void validator(){
		String email = inputter();
		int dotlocation = 0;
		int location = 0;
		boolean dot = false;
		boolean isEmail = false;
		boolean goodEnd = false;
		int emailLength = email.length();

		for(int i=0; i<emailLength; i++){ //loop to check every single char in the email
			char charInEmail = email.charAt(i);
			if(i>0 && charInEmail == '@'){ //if i > 0 and char is '@' then set a flag isEmail to true
				isEmail = true;
				location = i;

			}if(isEmail==true){
				if(i>location+1 && charInEmail=='.'){ //if flag isEmail is true and the location is 2 char from @ sets the flag dot to true
					dot = true;
					dotlocation = i;

				}
			}if(i==emailLength-1 && charInEmail!='.'){ //if the email doesn't end with . therefor it is valide
				goodEnd = true;

			}
		}
		if(dot==false || isEmail==false || goodEnd==false){//if one of the flags is false the email is invalide
			System.out.println("Your Email " + email + " is invalid");

		}else{
			System.out.println("Your Email " + email + " is valid");

		}

	}
}