import java.util.*;
/*This is a game that makes the user guess the value of a random number*/

public class GuessIt{

	public static void main(String[] args){
		int number = generator();
		System.out.println("Welcome to GuessIt!!\nThe game where YOU have to guess a number between 0 and 100.\nYou have 10 tries.");
		for(int tryCount=0; tryCount<10; tryCount++){
			int triesLeft = 10 - tryCount;
			System.out.println("\nYou have " + triesLeft + " tries left");
			int guess = inputter(1);
			System.out.println("\nYour guess is: " + guess);
			if(guess!=number){
				System.out.println("\nYou have guessed wrong!");
			}else{
				System.out.println("\nYou have guessed correcly!!");
				int playAgain = inputter(2);
				if(playAgain==1){
					main(null);

				}else{
					System.exit(0);

				}

			}
		}
		System.out.println("\nOut of tries!!!\nThe number was " + number +"\nBetter luck next time!");

	}
	private static int inputter(int n){
		int number = 0;
		if(n == 1){
			Scanner scGuess = new Scanner(System.in);
			System.out.print("Enter your guess: ");
			number = scGuess.nextInt();

		}if(n == 2){
			Scanner scGuess = new Scanner(System.in);
			System.out.print("Do you want to play again?\n(enter 1 to play again else to exit):");
			number = scGuess.nextInt();

		}
		return(number);

	}
	private static int generator(){
		Random rand = new Random();
		int number = rand.nextInt(101);//random number between 0 and 100 the 101 is not included
		return(number);

	}
}