import java.util.*;

public class MetricLength{

	public static void main(String[] args){
		final double CM_PER_INCH = 2.54;
		double inches = inputer();
		double centimeters = inches * CM_PER_INCH;
		System.out.println(String.format("%.2f", inches) + " inches is equal to " 
			+ String.format("%.2f", centimeters) + " centimeters");

	}
	private static double inputer(){
		Scanner scInches = new Scanner(System.in);
		System.out.print("Enter length in inches: ");
		double inches = scInches.nextDouble();

		return(inches);

	}


}