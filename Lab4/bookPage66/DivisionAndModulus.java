import java.util.*;

public class DivisionAndModulus{

	public static void main(String[] args){
		final int PENNIES_PER_QUARTER = 25;
		int pennies = inputter();

		int quarters = pennies / PENNIES_PER_QUARTER;
		int penniesLeftOver = pennies % PENNIES_PER_QUARTER;

		System.out.println("there is " + quarters + " quaters and " + penniesLeftOver + " left over pennies in " + pennies + " pennies");

	}
	private static int inputter(){
		Scanner scPennies = new Scanner(System.in);
		System.out.print("Enter amount of pennies: ");
		int pennies = scPennies.nextInt();
		return(pennies);

	}
}