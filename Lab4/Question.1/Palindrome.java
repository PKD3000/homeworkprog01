import java.util.*;
/*This program checks if the phrase inputted by the user is a palindrome*/

public class Palindrome{

	public static void main(String[] args){
		validator();

	}
	private static String inputter(){
		Scanner scPhrase = new Scanner(System.in);
		System.out.print("Enter your palindrome: ");
		String phrase = scPhrase.nextLine();
		return(phrase);

	}
	private static void validator(){
		String phrase = inputter();
		String phraseLC = phrase.toLowerCase();//removes uppercases
		String phraseNW = phraseLC.replaceAll("\\s+", "");//removes all whitespaces from string
		StringBuilder sb = new StringBuilder(phraseNW);
		String reversedPhrase = sb.reverse().toString(); //flips the String using StringBuilder

		if(phraseNW.equals(reversedPhrase)){// checks if its a palindrome
			System.out.println("Your Palindrome " + phrase + " is valid");

		}else{
			System.out.println("Your Palindrome " + phrase + " is invalid");

		}
	}
}