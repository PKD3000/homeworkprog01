import java.util.*;

public class Converters{

	public static void main(String[] args){
		int choice = intInputter();
		if(choice > 0 && choice <= 5){
			switch(choice){
				case 1: meterToFeet();
					break;
				case 2: cadToUsd();
					break;
				case 3: kmToMile();
					break;
				case 4: literToOunce();
					break;
				case 5: celsiusToKelvin(false);
					break;
			}
		}

	}
	private static int intInputter(){
		try{
			Scanner scChoice = new Scanner(System.in);
			System.out.println("Choose a converter:");
			System.out.println("1. Meters to feet.");
			System.out.println("2. CAD To USD.");
			System.out.println("3. Kilometers To Miles.");
			System.out.println("4. Liters To Ounces.");
			System.out.println("5. Celsius To Kelvin.");
			System.out.print("Choice:");
			int choice = scChoice.nextInt();
			if(choice <= 0  || choice > 5){
				System.out.println("Your choice is not an option.");
				main(null);
			}
			return(choice);

		}catch(InputMismatchException e){
			System.out.println("Make sure to enter a number without decimals.");
			main(null);

		}
		return(0);

	}
	private static double dbInputter(boolean cond){
		try{
			Scanner scDbValue = new Scanner(System.in);
			double dbValue = scDbValue.nextDouble();
			if(dbValue < 0){
				System.out.println("Your input cannot be less than 0.");
				main(null);
			}
			if(cond == true){
				if(dbValue == 0){ 
					celsiusToKelvin(true);
				}
			}else if(dbValue == 0){
				System.out.println("Your conversion is equal to 0.");
				System.exit(0);
			}
			return(dbValue);

		}catch(InputMismatchException e){
			System.out.println("Make sure to enter a number.");
			main(null);

		}
		return(0);

	}
	private static void meterToFeet(){
		System.out.println("### Meters To Feet Converter ###");
		final double RATIO = 3.28084;
		System.out.print("Meters:");
		double meters = dbInputter(false);
		if(meters > 0){
			double feet = meters * RATIO;
			System.out.println(meters + " meters is equal to " + feet + " feet");
		}
	}
	private static void cadToUsd(){
		System.out.println("### CAD To USD Converter ###");
		final double RATIO = 0.74;
		System.out.print("CAD: ");
		double cad = dbInputter(false);
		if(cad > 0){
			double usd = cad * RATIO;
			System.out.println(cad + " CAD is equal to " + usd + " USD");
		}

	}
	private static void kmToMile(){
		System.out.println("### Kilometers To Miles Converter ###");
		final double RATIO = 0.6213712;
		System.out.print("KM: ");
		double km = dbInputter(false);
		if(km > 0){
			double miles = km * RATIO;
			System.out.println(km + " Kilometers is equal to " + miles +" Miles");
		}

	}
	private static void literToOunce(){
		System.out.println("### Liter To Fluid Ounce Converter ###");
		final double RATIO = 35.19503;
		System.out.print("Liter: ");
		double liter = dbInputter(false);
		if(liter > 0){
			double ounce = liter * RATIO;
			System.out.println(liter + " Liters is equal to " + ounce +" Ounces");
		}
	}
	private static void celsiusToKelvin(boolean cond){
		if(cond == true){
			System.out.println("0 degrees Celsius is equal to 273.15 degrees Kelvin");

		}else{
			System.out.println("### Celsius To Kelvin Converter ###");
			final double ADDFORM = 273.15;
			System.out.print("Celsius: ");
			double celsius = dbInputter(true);
			if(celsius > 0){
				double kelvin = celsius + ADDFORM;
				System.out.println(celsius + " degrees Celsius is equal to " + kelvin +" degrees kelvin");
			}
		}
	}
}
