import java.util.*;

/*This program prompts the user for a number and check if it is a actually a number and is greater than 0.
Then it calculates the area of the circle*/

public class Circle{
	
	public static void main(String[] args){
		final double PI = Math.PI;
		double radius = inputer();
		if(radius > 0){
			double area = PI * Math.pow(radius,2);
			System.out.println("\nYou have entered all value correctly\n");
			System.out.println("The area of the circle is: " + String.format("%.2f", area));
		}
	}

	private static double inputer(){
		try{
			Scanner scRadius = new Scanner(System.in);
			System.out.print("Enter a radius: ");
			double radius = scRadius.nextDouble();
			if(radius <= 0.0){
				System.out.println("The radius cannot be equal or less than 0.");
				scRadius.reset();
				main(null);
			}
			return(radius);

			
		}catch(InputMismatchException e){
			System.out.println("Make sure to enter a number.");
			main(null);

		}
		return(0);

	}
}