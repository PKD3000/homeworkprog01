import java.util.*;

public class Celsius{

	public static void main(String[] args){
		int celsius = inputter();
		if(celsius != 0){
			String status  = (celsius > 0) ? "Above zero" : "Below zero";
			System.out.println(celsius + " degrees celsius is " + status);
		}
	}
	private static int inputter(){
		try{
			Scanner scCelsius = new Scanner(System.in);
			System.out.print("Enter degrees celsius: ");
			int celsius = scCelsius.nextInt();
			if(celsius == 0){
				System.out.println("0 degrees celsius is equal to zero.");
				System.exit(1);
			}
			return(celsius);

			
		}catch(InputMismatchException e){
			System.out.println("Make sure to enter a number without decimals.");
			main(null);

		}
		return(0);
	}
}