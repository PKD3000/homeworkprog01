import java.util.*;

public class BmiCal{

	public static void main(String[] args){
		double meters = converter(1);
		double kg = converter(2);
		String status = "";
		if(meters > 0 && kg > 0){
			double bmi = kg/(Math.pow(meters,2));
			if(bmi < 18){
				status = "Underweight";
			}
			if(bmi > 17 && bmi < 25){
				status = "Healthy";
			}
			if(bmi > 24 && bmi < 30){
				status = "Overweight";
			}
			if(bmi > 29 && bmi < 40){
				status = "Obese";
			}
			if(bmi > 39){
				status = "Extremely Obese";
			}
			System.out.println("Your BMI is: " + String.format("%.2f",bmi) + " You are: " + status);
			System.exit(1);
		}
	}
	private static double inputter(int n){
		try{
			if(n == 1){
				Scanner scFeet = new Scanner(System.in);
				System.out.print("Enter your height in feet: ");
				double feet = scFeet.nextDouble();
				if(feet <= 0){
					System.out.println("You have entered a imposible height.");
					main(null);
				}
				return(feet);
			}
			if(n == 2){
				Scanner scPounds = new Scanner(System.in);
				System.out.print("Enter your weight in pounds: ");
				double pounds = scPounds.nextDouble();
				if(pounds <= 0){
					System.out.println("You have entered a imposible height.");
					main(null);
				}
				return(pounds);
			}
		}catch(InputMismatchException e){
			System.out.println("Make sure to enter a number.");
			main(null);

		}
		return(0);
	}
	private static double converter(int n){
		final double FMRATIO = 0.3048;
		final double PKGRATIO = 0.45359237;
			if(n == 1){
				double feet = inputter(1);
				double meters = feet * FMRATIO;
				return(meters);
			}
			if(n == 2){
				double pounds = inputter(2);
				double kg = pounds * PKGRATIO;
				return(kg);
			}
			return(0);
		
	}

}